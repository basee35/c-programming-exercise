<!doctype html>
<html class="theme-5">
<meta charset="utf-8" />
<link href="../html-slideshow.bundle.min.css" rel="stylesheet" />
<link href="../style.css" rel="stylesheet" />
<script src="https://dbwebb.se/cdn/js/html-slideshow_v1.1.0.bundle.min.js"></script>

<title>Programming in C</title>

<script data-role="slide" type="text/html" data-markdown class="titlepage center">
# Programming in C
## Introduction to the C programming language
### Mikael Roos
</script>



<script data-role="slide" type="text/html" data-markdown>
# Agenda

* Hello World
* About the C language
* Features
* Stdlib
* Compile, link, execute

</script>



<script data-role="slide" type="text/html" data-markdown class="center">
# Language architectures

* C compiled to machine readable object code
    * Compile to each architecture
* Java compiles to byte code, interpreted by a JVM (virtual machine)
    * Compile once, JVM for each architecture
* Python, PHP, JavaScript - source interpreted by the "interpreter", line by line

</script>



<script data-role="slide" type="text/html" data-markdown class="center">
# The original

<figure>
<img src="img/languages-architecture.png" width="40%">
<figcaption>Architecture layers and steps involved in executing a program in a language.</figcaption>
</figure>

</script>



<script data-role="slide" type="text/html" data-markdown class="center">
# Hello World

```
#include <stdio.h>

void main()
{
    printf("Hello World\n");
}
```

</script>



<script data-role="slide" type="text/html" data-markdown class="center">
# Hello World

```
$ ls -l hello-world.c
-rw-r--r-- 1 mos mos  65 Feb 10 15:42 hello-world.c

$ gcc hello-world.c

$ ls -l hello-world.c a.out
-rwxr-xr-x 1 mos mos 17K Feb 10 15:50 a.out*

$ ./a.out
Hello World
```

</script>



<script data-role="slide" type="text/html" data-markdown class="center">
# The original

<figure>
<img src="img/Hello_World_Brian_Kernighan_1978.jpg" width="50%">
<figcaption>"Hello, World!" program by Brian Kernighan (1978).</figcaption>
</figure>

</script>



<script data-role="slide" type="text/html" data-markdown class="center">
# C is popular

<figure>
<img src="img/tiobe-top-ten-2021.png" width="100%">
<figcaption>The index from https://www.tiobe.com/tiobe-index/ showing the "most popular" programming languages.</figcaption>
</figure>

</script>



<script data-role="slide" type="text/html" data-markdown class="center">
# History of popularity

<figure>
<img src="img/tiobe-history.png" width="100%">
<figcaption>Popularity over time.</figcaption>
</figure>

</script>



<script data-role="slide" type="text/html" data-markdown>
# About Tiobe

* TIOBE Programming Community Index Definition

> "The ratings are calculated by counting hits of the most popular search engines. The search query that is used is:"

```
+"<language> programming"
```

> "The number of hits determines the ratings of a language."

<p class="footnote">Is the method valid to state what programming languages are popular?</p>

</script>



<script data-role="slide" type="text/html" data-markdown>
# C language

* Successor to B
* Developed 1972 at Bell laboratory
* To construct utilitites on Unix
* One of the most widely used programming languages
* Standard 1989 (ANSI C and by ISO)

</script>



<script data-role="slide" type="text/html" data-markdown class="center">
# Ken and Dennis

<figure>
<img src="img/Ken_Thompson_and_Dennis_Ritchie--1973.jpg" width="80%">
<figcaption>Dennis Ritchie (right), the inventor of the C programming language, with Ken Thompson.</figcaption>
</figure>

</script>



<script data-role="slide" type="text/html" data-markdown>
# Features

* General purpose
* Imperative (statements change a program state)
* Structured (control flow if, for, while, block)
* Procedural (organise in procedures/functions)
* Cross platform, portable, compiled
* Low level access to memory
* Map efficiently to machine instructions
* Easier to code and more portable than assembly...

</script>



<script data-role="slide" type="text/html" data-markdown>
# Standards

| Year | Standard |
|------|----------|
| 1972 | First    |
| 1978 | K&R C    |
| 1989/1990 | C89, ANSI C and ISO C |
| 1999 | C99 |
| 2011 | C11 |
| 2017 | C17 |
| > 2021 | C2x next major release |

</script>



<script data-role="slide" type="text/html" data-markdown>
# Reserved words C89

|          |         |         |          |          |          |
|----------|---------|---------|----------|----------|----------|
| auto     | default | float   | register | struct   | volatile |
| break    | do      | for     | return   | switch   | while    |
| case     | double  | goto    | short    | typedef  |          |
| char     | else    | if      | signed   | union    |          |
| const    | enum    | int     | sizeof   | unsigned |          |
| continue | extern  | long    | static   | void     |          |

</script>



<script data-role="slide" type="text/html" data-markdown>
# Control flow

* if/else if/else
* for
* do/while
* while
* switch/case/default
* break/continue

</script>



<script data-role="slide" type="text/html" data-markdown>
# Data types

* Static typed
    * Implicit conversions (for example int o float)
* char (characters)
* string (null terminated char arrays)
* short, int, long (integers)
* float, double (single/double precision)
* struct/union/enum
* Arrays (a[])
* Pointers (*, &amp;)
* const or #define

</script>



<script data-role="slide" type="text/html" data-markdown>
# Operators

* arithmetic: +, -, *, /, %
* assignment: =
* boolean logic: !, &amp;&amp;, ||
* conditional evaluation: ? :
* equality testing: ==, !=
* calling functions: ( )
* increment and decrement: ++, --
* and more...

</script>



<script data-role="slide" type="text/html" data-markdown>
# Assignment or equality?

* `if (a == b + 1)`
* `if (a = b + 1)`

</script>



<script data-role="slide" type="text/html" data-markdown>
# Functions

* Block of code
* Declares local variables
* Can access global variables
* Arguments by value or reference
* Return a value (or not)
* Recursive
* Pointer to a function

</script>



<script data-role="slide" type="text/html" data-markdown>
# Comments

* One row - //
* Many rows - /* ... */

</script>



<script data-role="slide" type="text/html" data-markdown>
# Standard library

* stdio.h - input and output
* ctype.h - test characters
* string.h - str* for string manipulation and mem* for memory
* math.h - mathematical functions
* stdlib.h -  number conversion, storage allocation

<p class="footnote">Include the header file and link with the library file.</p>

</script>



<script data-role="slide" type="text/html" data-markdown>
# Standard library...

* assert.h - for diagnostics
* stdarg.h - list function arguments of unknown number and type
* setjmp.h - jump around overriding ordinary stack
* signals.h - deal with interrupts ans signals
* time.h - date and time functions
* limits.h, float.h - constants for types

</script>



<script data-role="slide" type="text/html" data-markdown>
# Include files

```
$ `gcc -print-prog-name=cpp` -v
Target: x86_64-linux-gnu
Thread model: posix
gcc version 8.3.0 (Debian 8.3.0-6)
#include "..." search starts here:
#include <...> search starts here:
 /usr/lib/gcc/x86_64-linux-gnu/8/include
 /usr/local/include
 /usr/lib/gcc/x86_64-linux-gnu/8/include-fixed
 /usr/include/x86_64-linux-gnu
 /usr/include
```

<p class="footnote">Ask the preprocessor cpp where it looks for the include files.</p>

</script>



<script data-role="slide" type="text/html" data-markdown class="center">
# Include files...

```
$ ls /usr/include/st*
/usr/include/stab.h         /usr/include/stdlib.h
/usr/include/stdc-predef.h  /usr/include/string.h
/usr/include/stdint.h       /usr/include/strings.h
/usr/include/stdio.h        /usr/include/stropts.h
/usr/include/stdio_ext.h
```

<p class="footnote">The include files are available in the system folders, or where the compiler environment are installed and configured to look for them.</p>

</script>



<script data-role="slide" type="text/html" data-markdown class="center">
# Include files...

<figure>
<img src="img/stdlib.png" width="80%">
<figcaption>Check the source code. Review the include files to see what they offer.</figcaption>
</figure>

</script>



<!--
<script data-role="slide" type="text/html" data-markdown>
# Arguments and exit

```
int main (int argc, char *argv[])
{
    if (argc != 4) {
        fprintf(stderr, "Usage: %s <lower> <upper> <step>\n", argv[0]);
        exit(EXIT_FAILURE);
    }

    lower = strtol(argv[1], NULL, 10);
    upper = strtol(argv[2], NULL, 10);
    step = strtol(argv[3], NULL, 10);

    exit(EXIT_SUCCESS);
}
```

</script>
-->



<script data-role="slide" type="text/html" data-markdown>
# Memory management

* <u>Static memory allocation</u>: space for the object is provided in the binary at compile-time
* <u>Automatic memory allocation</u>: temporary objects can be stored on the stack, freed and reusable after the block is exited
* <u>Dynamic memory allocation</u>: blocks of memory of arbitrary size can be requested at run-time using library functions (malloc) from a region of memory called the heap; persist until freed

<p class="footnote">C lacks automatic garbage collection.</p>

</script>



<script data-role="slide" type="text/html" data-markdown>
# Stack and heap

* The binary has the code to execute (and statically allocated objects)
* Program counter executes the code in the binary
* Each function call, or variable declaration, is pushed to the stack
    * A return from a function pops the stack
* Dynamic memory allocation is done on the heap

</script>



<script data-role="slide" type="text/html" data-markdown>
# Compiling and linking

```
$ gcc hello-world.c

$ ./a.out
Hello World
```

</script>



<script data-role="slide" type="text/html" data-markdown>
# Compiling and linking...

```
$ gcc -c hello-world.c
-rw-r--r-- 1 mos mos 1.5K Feb 11 00:37 hello-world.o

$ gcc hello-world.o
-rwxr-xr-x 1 mos mos  17K Feb 11 00:38 a.out*
```

<p class="footnote">Compiling to the object file and then linking.</p>

</script>



<script data-role="slide" type="text/html" data-markdown>
# Compiler 4 phases

1. Pre-processing
1. Compilation
1. Assembly
1. Linking

<p class="footnote">Do it all at once or separete the phases.</p>

</script>



<script data-role="slide" type="text/html" data-markdown>
# Resulting files

```
$ file *
hello-world.c: C source, ASCII text

hello-world.o: ELF 64-bit LSB relocatable, x86-64, not stripped

a.out:         ELF 64-bit LSB pie executable, x86-64, not stripped
```

<p class="footnote">Using the file command one can examine the file types.</p>

</script>



<script data-role="slide" type="text/html" data-markdown class="center">
# Static linked

<figure>
<img src="img/static-linked.png" width="90%">
<figcaption>Dynamic linked depends on dynamic loaded libraries, statical linked has all in the executable.</figcaption>
</figure>

</script>



<script data-role="slide" type="text/html" data-markdown>
# Makefile

```
#!/usr/bin/env make

CC=gcc

main: main.c
	$(CC) -g -o main main.c

clean:
	rm -f *.o a.out main
```

</script>



<script data-role="slide" type="text/html" data-markdown>
# Compile warnings

```
$ make
gcc -g -o main main.c
main.c: In function ‘main’:
main.c:5:14: warning: division by zero [-Wdiv-by-zero]
     int a = 1/0;
              ^
main.c:6:14: warning: division by zero [-Wdiv-by-zero]
     return 1 / 0;
              ^

$ ./main
Floating point exception
```

</script>



<script data-role="slide" type="text/html" data-markdown>
# Compile errors

```
$ make
gcc  -o celc-fahr main.c
main.c: In function ‘main’:
main.c:13:12: error: expected expression before ‘)’ token
     while () fahr <= upper ) {
            ^
main.c:13:28: error: expected statement before ‘)’ token
     while () fahr <= upper ) {
                            ^
make: *** [Makefile:13: celc-fahr] Error 1
```

</script>



<script data-role="slide" type="text/html" data-markdown>
# Runtime errors

```
$ ./main
Segmentation fault
```

<p class="footnote">The program failed. Tough call.</p>

</script>



<script data-role="slide" type="text/html" data-markdown>
# Runtime errors

* Segmentation fault - attempted to access a restricted area of memory, memory access violation
* Bus error - fault raised by hardware, notifying that a process is trying to access memory that the CPU cannot physically address
* Core dump - a file containing the executable as it was in memory, useful for debugging

</script>



<script data-role="slide" type="text/html" data-markdown>
# Debugger

```
$ gdb ./celc-fahr2
GNU gdb (Debian 8.2.1-2+b3) 8.2.1
For help, type "help".
Reading symbols from ./celc-fahr2...done.
(gdb) run 10 20 5
Starting program: celc-fahr2 10 20 5
|------------|---------|
| Fahrenheit | Celcius |
|------------|---------|
|      10    |    -12  |
|      15    |     -9  |
|      20    |     -6  |
|------------|---------|
[Inferior 1 (process 8886) exited normally]
(gdb)
```

<p class="footnote">A debugger is usually built-in the IDE.</p>

</script>



<script data-role="slide" type="text/html" data-markdown>
# Development tool

* Command line tools
    * terminal, make, gcc, gdb, makedepend
* CodeBlocks IDE

</script>



<script data-role="slide" type="text/html" data-markdown class="center">
# CodeBlocks IDE

<figure>
<img src="img/codeblocks.png" width="80%">
<figcaption>The cross platform and free IDE known as Code::Blocks.</figcaption>
</figure>

</script>



<script data-role="slide" type="text/html" data-markdown>
# Man pages

* man 3 exit
* man 3 printf

```
$ man -k printf
asprintf (3)         - print to allocated string
dprintf (3)          - formatted output conversion
fprintf (3)          - formatted output conversion
```

</script>



<script data-role="slide" type="text/html" data-markdown class="center">
# Man pages

<figure>
<img src="img/man-printf.png" width="80%">
<figcaption>The man command section 3 gives you the man pages for C functions (on Linux and Mac).</figcaption>
</figure>

</script>



<script data-role="slide" type="text/html" data-markdown>
# ELF

```
$ make
gcc -g -c hello-world.c
gcc hello-world.o

$ ls -l
total 36K
-rw-r--r-- 1 mos mos  135 Feb 11 08:30 Makefile
-rwxr-xr-x 1 mos mos  19K Feb 11 08:30 a.out*
-rw-r--r-- 1 mos mos  127 Feb 11 08:26 hello-world.c
-rw-r--r-- 1 mos mos 5.7K Feb 11 08:30 hello-world.o
```

<p class="footnote">Is there a difference on the files?</p>

</script>



<script data-role="slide" type="text/html" data-markdown>
# ELF...

```
$ file *
Makefile:      a /usr/bin/env make script, ASCII text executable

a.out:         ELF 64-bit LSB executable, x86-64, version 1 (GNU/Linux), statically linked, for GNU/Linux 3.2.0, BuildID[sha1]=53821231c17d66d26f3b23813e542bc3ab0b8d33, not stripped

hello-world.c: C source, ASCII text

hello-world.o: ELF 64-bit LSB relocatable, x86-64, version 1 (SYSV), with debug_info, not stripped
```

<p class="footnote">Is there a difference on the files?</p>

</script>



<script data-role="slide" type="text/html" data-markdown class="center">
# ELF details

<figure>
<img src="img/elf-details.png" width="80%">
<figcaption>ELF (Executable and Linkable Format) is a common standard file format for executable files, object code, shared libraries, and core dumps.</figcaption>
</figure>

</script>



<script data-role="slide" type="text/html" data-markdown class="center">
# ELF header

<figure>
<img src="img/elf-header.png" width="90%">
<figcaption>ELF header displaying details on the content of the file using the command readelf.</figcaption>
</figure>

</script>



<script data-role="slide" type="text/html" data-markdown class="center">
# ELF file layout

<figure>
<img style="background-color: white; padding: 1em;" src="img/Elf-layout--en.svg" width="35%">
<figcaption>ELF file contains an ELF header, program header table, section header table and data referred to by entries in program/section header table.</figcaption>
</figure>

</script>



<script data-role="slide" type="text/html" data-markdown>
# ELF sections

* .text - where code lives
* .data - global tables, variables
* .bss - block starting symbols for uninitialized arrays and variable, filled with zeros
* .rodata - here are youre strings
* .comment & .note - comments made by the compiler/linker toolchain
* .stab & .stabstr - debugging symbols & similar information

</script>



<script data-role="slide" type="text/html" data-markdown class="center">
# ELF .text

<figure>
<img src="img/elf-text.png" width="90%">
<figcaption>This is the ELF section where your code is stored, you can view it using the command objdump.</figcaption>
</figure>

</script>



<script data-role="slide" type="text/html" data-markdown class="center">
# ELF .data .rodata

<figure>
<img src="img/elf-data.png" width="90%">
<figcaption>The ELF sections where .data (global tables, variables) and .rodata (strings) are stored.</figcaption>
</figure>

</script>



<script data-role="slide" type="text/html" data-markdown>
# Memory Layout of C Program

* When running/executing the program
* Loader loads the executable ELF into memory for execution
* Adding a heap and a stack (memory)
* Positioning and starting the program counter
* Reading instruction by instruction (cpu)

</script>



<script data-role="slide" type="text/html" data-markdown class="center">
# ELF executing

<figure>
<img style="background-color: white; padding: 1em;" src="img/Elfdiagram.png" width="50%">
<figcaption>The ELF is mapped onto memory with its segments and adding a stack and a heap (data).</figcaption>
</figure>

</script>



<script data-role="slide" type="text/html" data-markdown class="center">
# Memory Layout

<figure>
<img src="img/memoryLayoutC.jpg" width="70%">
<figcaption>The loader maps the object file onto memory.</figcaption>
</figure>

</script>

<!--

<script data-role="slide" type="text/html" data-markdown class="titlepage center">
# Exercises to do
## Homework to practice and learn the basics
</script>



<script data-role="slide" type="text/html" data-markdown>
# Exercise 1

```
$ make
gcc -o hello-world hello-world.c

$ ./hello-world
Hello World

$ echo $?
0
```

<p class="footnote">Create your own hello world and return a exit status of success.</p>

</script>



<script data-role="slide" type="text/html" data-markdown>
# Exercise 2

```
$ make
gcc  -o celc-fahr main.c

$ ./celc-fahr
|------------|---------|
| Fahrenheit | Celcius |
|------------|---------|
|       0    |    -17  |
|      20    |     -6  |
|      40    |      4  |
|      60    |     15  |
|      80    |     26  |
|     100    |     37  |
|     120    |     48  |
|     140    |     60  |
|------------|---------|
```

<p class="footnote">Do calculations and structured output to the terminal.</p>

</script>



<script data-role="slide" type="text/html" data-markdown>
# Exercise 3

```
$ make
gcc -g -Wall   -c -o main.o main.c
gcc -g -Wall   -c -o celcius_fahrenheit.o celcius_fahrenheit.c
gcc -g -Wall  -o celc-fahr2 ./main.o ./celcius_fahrenheit.o

$ ./celc-fahr2
Usage ./celc-fahr2 <lower> <upper> <step>

$ echo $?
1
```

<p class="footnote">Split into function in its own module and take arguments from the command line.</p>

</script>



<script data-role="slide" type="text/html" data-markdown>
# Exercise 3...

```
$ ./celc-fahr2 10 100 50
|------------|---------|
| Fahrenheit | Celcius |
|------------|---------|
|      10    |    -12  |
|      60    |     15  |
|------------|---------|

$ echo $?
0
```

<p class="footnote">Make it a bit more flexible.</p>

</script>

-->


<script data-role="slide" type="text/html" data-markdown>
# Do it on your own

```
#include <stdio.h>

int main (int argc, char *argv[])
{
    printf("Hello World\n");

    exit(EXIT_SUCCESS);
}
```

<p class="footnote">Create your own hello world and return a exit status of success.</p>

</script>



<script data-role="slide" type="text/html" data-markdown>
# Do it on your own...

```
$ make
gcc -o hello-world hello-world.c

$ ./hello-world
Hello World

$ echo $?
0
```

<p class="footnote">Or just hit F5 in your IDE to "build and run".</p>

</script>



<script data-role="slide" type="text/html" data-markdown class="titlepage center">
# The end
</script>



<script data-role="slide" type="text/html" data-markdown>
</script>

</html>
