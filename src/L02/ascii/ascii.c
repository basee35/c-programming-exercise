#include <stdio.h>
#include <stdlib.h>
#include <time.h>

const char* marvin =    \
"     (____)\n"         \
"     (_oo_)\n"         \
"       (O)\n"          \
"     __||__     )\n"   \
"  []/_______[] /\n"    \
"  / ________/ /\n"     \
" /    /___/\n"         \
"(    /___/\n"          \
;

int main (int argc, char *argv[])
{
    printf("This is my C programming mascot\n%s\n", marvin);

    exit(EXIT_SUCCESS);
}
