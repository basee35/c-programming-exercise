#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int main (int argc, char *argv[])
{
    int min, max;

    printf("Enter a min integer value:\n");
    scanf("%d", &min);

    printf("Enter a max integer value:\n");
    scanf("%d", &max);

    printf("You entered min=%d and max=%d\n", min, max);

    srand(time(NULL));               // Initialization, call only once.
    int r = rand() % max + min + 1;  // Returns a pseudo-random integer between
                                     // min and max.

    printf("A random number between these are %d.\n", r);

    exit(EXIT_SUCCESS);
}
