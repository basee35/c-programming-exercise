Homework assignment
=======================

This is the voluntary homework assignment that will let you practice small exercises.

[[_TOC_]]



Try out and inspect the example programs
-----------------------

Try out the example programs in this folder. Compile them, exeute them.

Inspect their source code, can you understand all parts of the code and can you understand what the program is doing?

Feel free to copy them and try to make your own updates to them.



Loop until the entered value is 42
-----------------------

Use the example program "scanf" and enhance to continue to read integers until the user enters the value 42. Then print out "You found the answer!" and exit the program.

A do-while loop might be of use here.



Throw a hand of dices
-----------------------

Take the example program "random" and rewrite it to throw 6 dices and print the values of all dices together with the sum of all dices.

Enhance the program to enable throwing the set of dices one more, and once more again, until you enter a the digit 0;



Ascii art
-----------------------

Play around with the example program "ascii" and make your own ascii art supervisor that will be your guideance all through your journey to become a master in C.

```
$ ./a.out
This is my C programming mascot
     (____)
     (_oo_)
       (O)
     __||__     )
  []/_______[] /
  / ________/ /
 /    /___/
(    /___/

```
