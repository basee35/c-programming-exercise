#include <stdio.h>
#include <stdlib.h>

#include "celcius_fahrenheit.h"

int main (int argc, char *argv[])
{
    int lower, upper, step;

    if (argc != 4) {
        fprintf(stderr, "Usage: %s <lower> <upper> <step>\n", argv[0]);
        exit(EXIT_FAILURE);
    }

    lower = strtol(argv[1], NULL, 10);
    upper = strtol(argv[2], NULL, 10);
    step = strtol(argv[3], NULL, 10);

    print_fahrenheit_celcius(lower, upper, step);

    exit(EXIT_SUCCESS);
}
