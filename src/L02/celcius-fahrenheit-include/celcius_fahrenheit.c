#include <stdio.h>

#include "celcius_fahrenheit.h"

void print_fahrenheit_celcius(int lower, int upper, int step)
{
    int fahr, celcius;

    printf("|------------|---------|\n");
    printf("| Fahrenheit | Celcius |\n");
    printf("|------------|---------|\n");

    fahr = lower;
    while ( fahr <= upper ) {
        celcius = 5 * (fahr - 32) / 9;
        printf("| %7d    | %6d  |\n", fahr, celcius);
        fahr += step;
    }

    printf("|------------|---------|\n");
}
