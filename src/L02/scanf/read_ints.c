#include <stdio.h>
#include <stdlib.h>

int main (int argc, char *argv[])
{
    int val;

    printf("Enter a integer value:\n");
    scanf("%d", &val);

    printf("You entered a value = %d\n", val);

    exit(EXIT_SUCCESS);
}
