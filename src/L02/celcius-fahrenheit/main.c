#include <stdio.h>

void main()
{
    int fahr, celcius;
    int lower, upper, step;

    lower = 0;
    upper = 150;
    step = 20;

    printf("|------------|---------|\n");
    printf("| Fahrenheit | Celcius |\n");
    printf("|------------|---------|\n");

    fahr = lower;
    while ( fahr <= upper ) {
        celcius = 5 * (fahr - 32) / 9;
        printf("| %7d    | %6d  |\n", fahr, celcius);
        fahr += step;
    }

    printf("|------------|---------|\n");
}
